<?php

namespace App\Repositories;

use App\User;

class TaskRepository
{
    /**
     * Grabs all tasks per user
     * @param  User $user
     * @return boolean
     */
	public function forUser(User $user)
	{
		return $user->tasks()->orderBy('created_at', 'asc')->get();
	}
}